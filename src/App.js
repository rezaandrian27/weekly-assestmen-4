import "./App.css";
import "./components/css/style.css";

function App() {
  return (
    <div className="container">
      <div className="box">
        <div className="login-left">
          <h1>
            GOT MARKETING? <br />
            ADVANCE YOUR <br />
            BUSINESS INSIGHT.
          </h1>
          <p>Fill out the form and receive our award winning newsletter.</p>
        </div>
      </div>
      <div className="form-up">
        <form>
          <div>
            <label>Name</label>
            <input type="text" />
          </div>
          <div>
            <label>Email</label>
            <input type="text" />
          </div>
          <div>
            <button>Sign me up</button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default App;
